from PIL import Image
import os
import math
import random

dirData = 'C:\\Users\\user\\Desktop\\Dani\\Programmazione\\phyton\\ioc\\data'

# ARGUMENTS
step = 10  # step applied in the for that find points
# END

nm = '\\uova'
name = nm + '.png'

img = Image.open(dirData + name)

width, height = img.size

points = []

def isPoint(coor):
	x,y = coor
	b , n = (0,0)
	for ty in range(y-int(step/2), y+int(step/2)):
		for tx in range(x-int(step/2),x+int(step/2)):
			if ty >= 0 and ty < height and tx >= 0 and tx < width:
				if img.getpixel((x,y)) == 0:
					b+=1
				else:
					n+=1
	return not b>n

w = int(width/step)
h = int(height/step)

for y in range(0,height,step):
	for x in range(0,width,step):
		if isPoint(x,y):
			points.append((int(x/step),int(y/step)))

centroids = []

class C:
	coor = 0
	name = ''
	def __init__(self,coord,nm):
		coor = coord
		name = nm



def dist(a,b):
	xa,ya = a
	xb,yb = b
	return (xa-xb)**2 + (ya-yb)**2

def assignCentroid(coor):
	min = 10000
	for c in centroids:
		t = dist(c.coor,coor)
		if min>t:
			min=t

def cluster(k):

	p = points

	for i in range(k):
		centroids.append(C((random.randint(0,w),random.randint(0,h)),str(i)))
	for q in p:
		assignCentroid(q)

