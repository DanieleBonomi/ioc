import os
from PIL import Image
import math

dirData = 'C:\\Users\\user\\Desktop\\Dani\\Programmazione\\phyton\\ioc\\data\\'

nm = 'test'
name = nm + '.jpg'
tImage = Image.open(dirData + name)
width, height = tImage.size
# egg = (191,90,50)
# egg = (128,78,43)
egg = (0,0,0)



def comparePixel(n):
	dist = (n[0]+n[1]+n[2])/3
	return dist


def average(x, y, img):
	r, g, b = (0,0,0)
	toDivide = 0
	for yn in range(-1, 1):
		for xn in range(-1, 1):
			if (x + xn < width and y + yn < height and x + xn > 0 and y + yn > 0):
				t = img.getpixel((x+xn,y+yn))
				r += t[0]
				g += t[1]
				b += t[2]
				toDivide += 1
	r /= toDivide
	g /= toDivide
	b /= toDivide
	return (math.ceil(r), math.ceil(g), math.ceil(b))


def smooth(name):
	read = Image.open(name)
	put = Image.open(name)
	for y in range(height):
		for x in range(width):
			n = average(x,y,read)
			put.putpixel((x,y),(n[0],n[1],n[2]))
	return put


def outputImages(name):
	for n in range(30, 80, 5):
		img = Image.open(name)
		for y in range(height):
			for x in range(width):
				if comparePixel(img.getpixel((x,y))) > n:
					img.putpixel((x,y),(255,255,255))
				else:
					img.putpixel((x,y),(0,0,0))
		img.save(dirData + nm+'_'+str(n)+'.jpg')


def getBrightness (name):
	img = Image.open(name)
	br = []
	for i in range(256):
		br.append(0)
	for y in range(height):
		for x in range(width):
			br[img.getpixel((x,y))] += 1
	return br


